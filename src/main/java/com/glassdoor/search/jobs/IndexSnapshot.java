package com.glassdoor.search.jobs;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Calendar;

/**
 * Created by ye.fan on 4/10/17.
 */
public class IndexSnapshot {
    private static final Logger logger = LoggerFactory.getLogger(IndexSnapshot.class);

    public static final String AWS_PROFILE_NAME = "snapshot";
    public static final String AWS_PROFILE_FILE = "/usr/local/solr/.aws/config";

    public static final String INDEX_FOLDER = "/srv/solr";

    public static final String INDEX_SNAPSHOT_S3_BUCKET = "gd-de-dp-pr-ingest-jobcoverage";

    public static void main(String [] args) {

        // retrieve shard info
        String shard = getShardName(args);
        logger.info("Found shard: " + shard);
        String tempFilePath = "/root/" + shard;

        // create index snapshot file
        IndexReader indexReader = new IndexReader(shard);
        indexReader.generateIndexSnapShot(tempFilePath);

        // upload snapshot file
        String dateStr = new SimpleDateFormat("yyyy-MM-dd").format(Calendar.getInstance().getTime());
        S3MultipartUploader smu = new S3MultipartUploader(AWS_PROFILE_FILE, AWS_PROFILE_NAME, INDEX_SNAPSHOT_S3_BUCKET);
        smu.upload(tempFilePath, dateStr + "/" + shard);
        smu.shutdown();

        // clean up
        File file = new File(tempFilePath);
        if (file.exists()) {
            file.delete();
        }
    }

    private static String getShardName(String [] args) {
        String shardName = "";
        if (args.length == 1) {
            shardName = args[0];
        } else {
            File dir = new File(INDEX_FOLDER);
            File[] filesList = dir.listFiles();
            if (!((filesList == null) || (filesList.length == 0))) {
                for (File file : filesList) {
                    if (file.getName().startsWith("jobs")) {
                        shardName = file.getName();
                        break;
                    }
                }
            }
        }

        return shardName;
    }
}
