package com.glassdoor.search.jobs;

import org.apache.solr.client.solrj.SolrClient;
import org.apache.solr.client.solrj.SolrQuery;
import org.apache.solr.client.solrj.SolrRequest;
import org.apache.solr.client.solrj.SolrServerException;
import org.apache.solr.client.solrj.impl.HttpSolrClient;
import org.apache.solr.client.solrj.response.QueryResponse;
import org.apache.solr.common.SolrDocument;
import org.apache.solr.common.SolrDocumentList;
import org.apache.solr.common.params.CommonParams;
import org.json.simple.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.*;

/**
 * Created by ye.fan on 4/7/17.
 */
public class IndexReader {
    private final Logger logger = LoggerFactory.getLogger(IndexReader.class);

    private static final String SOLR_URL = "http://127.0.0.1:8983/solr/";
    private SolrClient solrClient;

    public IndexReader(String shard) {
        solrClient = new HttpSolrClient(SOLR_URL + shard);
    }

    public IndexReader(String host, String shard) {
        solrClient = new HttpSolrClient(host + shard);
    }

    public boolean freezeIndex() {
        logger.info("Suspending index replication...");

        SolrQuery solrQuery = new SolrQuery();

        solrQuery.setRequestHandler("/replication");
        solrQuery.set("command", "disablepoll");

        QueryResponse queryResponse = null;
        try {
            queryResponse = solrClient.query(solrQuery, SolrRequest.METHOD.POST);
        } catch (Exception e) {
            logger.error(e.getMessage());
            return false;
        }

        int status = (Integer) queryResponse.getResponseHeader().get("status");

        return (status == 0) ? true : false;
    }

    public boolean activateIndex() {
        logger.info("Resuming index replication...");

        SolrQuery solrQuery = new SolrQuery();

        solrQuery.setRequestHandler("/replication");
        solrQuery.set("command", "enablepoll");

        QueryResponse queryResponse = null;
        try {
            queryResponse = solrClient.query(solrQuery, SolrRequest.METHOD.POST);
        } catch (Exception e) {
            logger.error(e.getMessage());
            return false;
        }

        int status = (Integer) queryResponse.getResponseHeader().get("status");

        return (status == 0) ? true : false;
    }

    public void generateIndexSnapShot(String filePath) {

        freezeIndex();

        logger.info("Generating index snapshot at: " + filePath + "...");

        long startTime = System.currentTimeMillis();

        File file= new File(filePath);

        if (file.exists()) {
            file.delete();
        }

        FileOutputStream fos = null;
        try {
            fos = new FileOutputStream(file);


            BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(fos));

            String cursorMark = "#";
            String nextCursorMark = "*";
            int counter = 0;
            while (!cursorMark.equals(nextCursorMark)) {
                cursorMark = nextCursorMark;
                SolrQuery solrQuery = new SolrQuery();
                solrQuery.setRequestHandler("/select");
                solrQuery.set(CommonParams.ROWS, 10000);
                solrQuery.set(CommonParams.Q, "*:*");
                solrQuery.set(CommonParams.SORT, "uniqueId asc");
                solrQuery.set("cursorMark", cursorMark);
                solrQuery.set("group", "false");
                solrQuery.set("dedup", "false");
                solrQuery.set(CommonParams.FL, "field(aoAdvertiserType),field(aoPartnerId),jobListingId,cityId,stateId,countryId,gocId,mGocId,jobTitle,gdApplyEnabled,jobReqId,jobSourceId,importConfigId,easyApplyEnabled,isSponsored,employerId,aoId,aoSponsorshipLevel,hideFromSearchTypes");

                QueryResponse queryResponse = solrClient.query(solrQuery, SolrRequest.METHOD.POST);

                SolrDocumentList solrDocuments = queryResponse.getResults();
                for (SolrDocument solrDocument : solrDocuments) {

                    JSONObject jsonDoc = new JSONObject();
                    for (String key : solrDocument.keySet()) {
                        jsonDoc.put(key, solrDocument.getFieldValue(key));
                    }
                    bw.write(jsonDoc.toString());
                    bw.newLine();
                }

                nextCursorMark = (String) queryResponse.getResponse().get("nextCursorMark");
                logger.debug(counter + ": " + cursorMark);
                counter++;
            }

            bw.close();

        } catch (FileNotFoundException e) {
            logger.error("Unable to create file: " + filePath);
        } catch (IOException e) {
            logger.error(e.getMessage());
        } catch (SolrServerException e) {
            logger.error(e.getMessage());
        }

        activateIndex();

        long endTime = System.currentTimeMillis();

        logger.info("Elapsed time in seconds: " + (endTime - startTime)/1000);
    }
}
