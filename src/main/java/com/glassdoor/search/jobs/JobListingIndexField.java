package com.glassdoor.search.jobs;

import com.google.common.collect.ImmutableMap;
import com.google.common.collect.Lists;

import java.util.List;
import java.util.Map;

public enum JobListingIndexField {

    JOBLISTING_ID("jobListingId", FieldType.LONG, ReturnType.STORED_FIELD),
    JOBREQ_ID("jobReqId", FieldType.LONG, ReturnType.STORED_FIELD),
    JOB_TITLE("jobTitle", FieldType.STRING, ReturnType.STORED_FIELD),
    EMPLOYER_ID("employerId", FieldType.INTEGER, ReturnType.STORED_FIELD),
    CITY_ID("cityId", FieldType.INTEGER, ReturnType.STORED_FIELD),
    STATE_ID("stateId", FieldType.INTEGER, ReturnType.STORED_FIELD),
    COUNTRY_ID("countryId", FieldType.INTEGER, ReturnType.STORED_FIELD),
    GDAPPLY_ENABLED("gdApplyEnabled", FieldType.BOOLEAN, ReturnType.STORED_FIELD),
    IS_SPONSORED("isSponsored", FieldType.BOOLEAN, ReturnType.STORED_FIELD),
    JOB_SOURCE_ID("jobSourceId", FieldType.INTEGER, ReturnType.STORED_FIELD),
    IMPORT_CONFIG_ID("importConfigId", FieldType.INTEGER, ReturnType.STORED_FIELD),
    AO_ID("aoId", FieldType.INTEGER, ReturnType.STORED_FIELD),
    EASYAPPLY_ENABLED("easyApplyEnabled", FieldType.BOOLEAN, ReturnType.STORED_FIELD),
    AO_SPONSORSHIP_LEVEL("aoSponsorshipLevel", FieldType.STRING, ReturnType.STORED_FIELD),
    GOC_ID("gocId", FieldType.INTEGER, ReturnType.STORED_FIELD),
    MGOC_ID("mGocId", FieldType.INTEGER, ReturnType.STORED_FIELD),
    AO_ADVERTISER_TYPE("aoAdvertiserType", FieldType.STRING, ReturnType.STORED_FIELD),
    AO_PARTNER_ID("aoPartnerId", FieldType.INTEGER, ReturnType.STORED_FIELD)
    ;

    public enum FieldType {
        BOOLEAN,
        INTEGER,
        LONG,
        STRING,
        OTHER,
    };

    public enum ReturnType {
        NO_RETURN,
        STORED_FIELD,
        ANALYZED_FIELD
    }

    private static final Map<String, JobListingIndexField> fieldName2Enum;

    private String fieldName;
    private FieldType fieldType;
    private ReturnType returnType; // i.e. should we seek the field from solr?

    static {
        ImmutableMap.Builder<String, JobListingIndexField> fieldName2Enum_mapBuilder =
                new ImmutableMap.Builder<String, JobListingIndexField>();

        for (JobListingIndexField indexField : JobListingIndexField.values()) {
            fieldName2Enum_mapBuilder.put(indexField.fieldName, indexField);
        }
        fieldName2Enum = fieldName2Enum_mapBuilder.build();
    }

    JobListingIndexField(String fieldName, FieldType fieldType, ReturnType returnType) {
        this.fieldName = fieldName;
        this.fieldType = fieldType;
        this.returnType = returnType;
    }

    public String fieldName() {
        return fieldName;
    }

    public String returnFieldName(){
        if(getReturnType() == ReturnType.ANALYZED_FIELD){
            return "field("+fieldName+")";
        } else{
            return fieldName;
        }
    }

    public FieldType fieldType() {
        return fieldType;
    }

    public ReturnType getReturnType() {
        return returnType;
    }

    public static JobListingIndexField fromFieldName(String fieldName) {
        return fieldName2Enum.get(fieldName);
    }

    public static String fieldName(String name) {
        try {
            return JobListingIndexField.valueOf(name).fieldName();
        } catch (IllegalArgumentException e) {
            // Name not matching any enum
            return null;
        }
    }

    public static List<String> askedByClientsFieldNames() {
        List<String> returnFields = Lists.newArrayList();

        for (JobListingIndexField field : JobListingIndexField.values()) {
            if (field.getReturnType() != null && field.getReturnType() == ReturnType.STORED_FIELD) {
                returnFields.add(field.returnFieldName());
            }
        }

        return returnFields;
    }

}