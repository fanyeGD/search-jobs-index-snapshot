package com.glassdoor.search.jobs;

import com.amazonaws.AmazonClientException;
import com.amazonaws.auth.profile.ProfileCredentialsProvider;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3ClientBuilder;
import com.amazonaws.services.s3.transfer.TransferManager;
import com.amazonaws.services.s3.transfer.TransferManagerBuilder;
import com.amazonaws.services.s3.transfer.Upload;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;

/**
 * Created by ye.fan on 4/11/17.
 */
public class S3MultipartUploader {
    private final Logger logger = LoggerFactory.getLogger(S3MultipartUploader.class);

    private TransferManager tm;
    private String bucketName;

    public S3MultipartUploader(String profilePath, String profileName, String bucketName) {
        AmazonS3 client = AmazonS3ClientBuilder.standard().withCredentials(new ProfileCredentialsProvider(profilePath, profileName)).build();
        this.tm = TransferManagerBuilder.standard().withS3Client(client).build();
        this.bucketName = bucketName;
    }

    public void upload(String filePath, String key) {
        logger.info("Uploading " + key + "...");
        Upload upload = tm.upload(
                bucketName, key, new File(filePath));

        try {
            upload.waitForCompletion();
            logger.info("Upload complete.");
        } catch (AmazonClientException e) {
            logger.error("Unable to upload file, upload was aborted.");
            e.printStackTrace();
        } catch (InterruptedException e) {
            logger.error("Unable to upload file, upload was aborted.");
            e.printStackTrace();
        }
    }

    public void shutdown() {
        this.tm.shutdownNow();
    }
}
